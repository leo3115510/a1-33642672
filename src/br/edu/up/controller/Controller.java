package br.edu.up.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import br.edu.up.model.Aluno;

public class Controller {
    private List<Aluno> alunos;

    public Controller() {
        this.alunos = new ArrayList<>();
    }

    public void carregarAlunos(String arquivoEntrada) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(arquivoEntrada));
        String linha;

        // Ignorar a primeira linha (cabeçalho)
        br.readLine();

        while ((linha = br.readLine()) != null) {
            // Dividir a linha pelo ponto e vírgula para obter os campos
            String[] campos = linha.split(";");

            // Extrair matrícula, nome e nota
            int matricula = Integer.parseInt(campos[0].trim());
            String nome = campos[1].trim();
            double nota = Double.parseDouble(campos[2].trim().replace(",", ".")); // Ajuste para tratar ',' como decimal

            // Criar objeto Aluno e adicionar à lista
            Aluno aluno = new Aluno(matricula, nome, nota);
            alunos.add(aluno);
        }

        br.close();
    }

    public void gerarRelatorio(String arquivoSaida) throws IOException {
        // Calcular estatísticas
        int quantidadeTotal = alunos.size();
        int aprovados = 0;
        int reprovados = 0;
        double menorNota = Double.MAX_VALUE;
        double maiorNota = Double.MIN_VALUE;
        double somaNotas = 0.0;

        for (Aluno aluno : alunos) {
            double nota = aluno.getNota();
            somaNotas += nota;

            if (nota >= 6.0) {
                aprovados++;
            } else {
                reprovados++;
            }

            if (nota < menorNota) {
                menorNota = nota;
            }

            if (nota > maiorNota) {
                maiorNota = nota;
            }
        }

        double mediaGeral = somaNotas / quantidadeTotal;

        PrintWriter pw = new PrintWriter(new FileWriter(arquivoSaida));

        pw.println("Quantidade de alunos na turma: " + quantidadeTotal);
        pw.println("Quantidade de alunos aprovados: " + aprovados);
        pw.println("Quantidade de alunos reprovados: " + reprovados);
        pw.println("Menor nota da turma: " + menorNota);
        pw.println("Maior nota da turma: " + maiorNota);
        pw.println("Média geral das notas: " + mediaGeral);

        pw.close();
    }
}
