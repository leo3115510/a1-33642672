import java.io.IOException;

import br.edu.up.controller.Controller;

public class Main {
    public static void main(String[] args) {
        String arquivoEntrada = "alunos.csv";
        String arquivoSaida = "resumo.csv";

        Controller controller = new Controller();

        try {
            controller.carregarAlunos(arquivoEntrada);
            controller.gerarRelatorio(arquivoSaida);
            System.out.println("Processamento concluído. Resultados salvos em " + arquivoSaida);
        } catch (IOException e) {
            System.err.println("Erro ao processar os arquivos: " + e.getMessage());
        }
    }
}
